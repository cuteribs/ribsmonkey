// ==UserScript==
// @name			什么值得买 编辑器增强
// @namespace		http://tampermonkey.net/
// @version			0.2
// @description		编辑器 拖拽图片上传 粘贴图片上传 粘贴图片地址上传
// @author			cuteribs
// @match			https://post.smzdm.com/tougao*
// @match			https://post.smzdm.com/edit/*
// @match			https://test.smzdm.com/p/*/submit*
// @match			https://test.smzdm.com/p/*/edit/*
// @icon 			https://www.smzdm.com/favicon.ico
// ==/UserScript==

(function () {
	let initEdit = () => {
		let ue = UE.getEditor('yuanchuang');

		ue.ready(() => {
			let id = $('#id').val();
			let $uEditor = $('#ueditor_0');
			let $uDocument = $($uEditor[0].contentWindow.document);

			$uDocument.on('dragstart', e => e.preventDefault());
			$uDocument.on('dragover', e => e.preventDefault());

			$uDocument.off('drop').on('drop', e => {
				e.preventDefault();
				let files = e.originalEvent.dataTransfer.files;
				uploadImages(id, files, e.target);
			});

			$uDocument.on('paste', e => {
				let clipboardData = e.originalEvent.clipboardData;
				let target = $uDocument.find('body.view')[0];

				switch (e.target) {
					case 'H2':
					case 'H3':
					case 'P':
						target = e.target;
						break;
				}

				if (clipboardData.files.length > 0) {
					// System Clipboard Files
					uploadImages(id, clipboardData.files, target);
				} else if (clipboardData.items.length > 0) {
					// HTML Clipboard Format
					let item;

					for (let i = 0; i < clipboardData.items.length; i++) {
						if (clipboardData.items[i].type == 'text/html') {
							item = clipboardData.items[i];
							break;
						}
					}

					if(!item) {
						return;
					}

					item.getAsString(s => {
						let matches = s.match(/<img.*?\s+src=(?:'|")([^'">]+)(?:'|")/);

						if(matches.length == 2){
							let url = matches[1];
							let mimeType;

							switch (str.substr(str.lastIndexOf('.'))) {
								case '.jpg':
									mimeType = 'image/jpg';
									break;
								case '.gif':
									mimeType = 'image/gif';
									break;
								default:
									mimeType = 'image/png';
									break;
							}

							downloadImage(str, mimeType, blob => uploadImages(id, [blob], target));
							return false;
						}
					});
				}
				// #endregion
			});
		});
	};

	let downloadImage = (url, mimeType, callback) => {
		let image = new Image();
		image.setAttribute('crossOrigin', 'anonymous');

		image.onload = e => {
			let canvas = document.createElement('canvas');
			canvas.width = e.target.width;
			canvas.height = e.target.height;
			let ctx = canvas.getContext('2d');
			ctx.drawImage(e.target, 0, 0);
			let dataUriSplits = canvas.toDataURL(mimeType).split(',');
			let byteStr;

			if (dataUriSplits[0].indexOf('base64') >= 0) {
				byteStr = atob(dataUriSplits[1]);
			} else {
				byteStr = unescape(dataUriSplits[1]);
			}

			let mimeStr = dataUriSplits[0].split(':')[1].slice(';')[0];
			let ia = new Uint8Array(byteStr.length);

			for (let i = 0; i < byteStr.length; i++) {
				ia[i] = byteStr.charCodeAt(i);
			}

			return callback(new Blob([ia], { type: mimeStr }));
		};

		image.src = url;
	};

	let uploadImages = (id, files, target) => {
		for (let i = 0; i < files.length; i++) {
			let url = `https://post.smzdm.com/ajax_res?action=uploadImg&id=${id}&uid=&key=D7326DC2462053A1334080DA5490537C`;
			let data = new FormData();
			data.append('id', `WU_FILE_${i}`);
			data.append('name', files[i].name);
			data.append('type', files[i].type);
			data.append('lastModifiedDate', files[i].lastModifiedDate);
			data.append('size', files[i].size);
			data.append('imgFile', files[i]);

			$.ajax({
				type: 'POST',
				url: url,
				data: data,
				dataType: 'json',
				processData: false,
				contentType: false
			}).done(result => {
				if (result && result.error == 0) {
					let $img = $(`<p><img src="${result.url}" alt="" title="" _src="${result.url}" /></p>`);
					console.log('target: ' + target.tagName);

					switch (target.tagName) {
						case 'H2':
						case 'H3':
						case 'P':
							$img.insertAfter($(target));
							break;
						default:
							$(target).append($img);
							break;
					}
				} else {
					window.alert(result.message);
				}
			});
		}
	};

	initEdit();
})();