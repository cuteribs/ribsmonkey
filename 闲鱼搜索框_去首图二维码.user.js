// ==UserScript==
// @name         闲鱼搜索框+去首图二维码
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  还原右上搜索框, 清除宝贝首图二维码
// @author       cuteribs
// @match        https://2.taobao.com/*
// @match        https://s.2.taobao.com/*
// @match        https://trade.2.taobao.com/*
// @match        http://s.ershou.taobao.com/*
// @grant        none
// @require      http://code.jquery.com/jquery-latest.js
// @icon         https://www.taobao.com/favicon.ico
// ==/UserScript==

(function () {
    let form = '<div class="idle-search"><form method="get" action="//s.2.taobao.com/list/list.htm" name="search" target="_top"><input class="input-search" id="J_HeaderSearchQuery" name="q" type="text" value="" placeholder="搜闲鱼" /><input type="hidden" name="search_type" value="item" autocomplete="off" /><input type="hidden" name="app" value="shopsearch" autocomplete="off" /><button class="btn-search" type="submit"><i class="iconfont">&#xe602;</i><span class="search-img"></span></button></form></div>';
    $('#J_IdleHeader').append(form);
    $('.mau-guide').remove();
    $('#popUp-div').remove();
    window.setInterval(() => {$('.download-layer').remove();}, 1000);
})();
